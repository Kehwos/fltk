from setuptools import find_packages, setup
from fltk import __version__

with open("README.md", "r") as fh:
    long_description = fh.read()

setup(
    name="fltk",
    version=__version__,
    author="Équipe pédagogique de l'Institut Gaspard Monge",
    author_email="antoine.meyer@univ-eiffel.fr",
    description="Eiffel Tk, une surcouche simplifiée à tkinter pour l'enseignement",
    long_description=long_description,
    long_description_content_type="text/markdown",
    keywords="simple,tkinter,canvas,gui",
    url="https://framagit.org/AntoineMeyer/fltk",
    packages=find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
        "Intended Audience :: Education",
        "Topic :: Education",
    ],
    python_requires=">=3.7",
)
