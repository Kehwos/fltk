.. fltk documentation master file, created by
   sphinx-quickstart on Sun Nov 16 10:24:56 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Documentation du module fltk
==================================

.. currentmodule:: fltk

.. warning:: Ceci est la documentation de la version 2023.1 du module ``fltk``
   (suivre :download:`ce lien <../fltk.py>` pour télécharger cette version).

Le module ``fltk`` est une *bibliothèque graphique simplifiée*
utilisant le module de conception d'interfaces graphiques
:mod:`tkinter` fourni avec Python.

Ce module est dérivé du module ``iutk`` créé en 2013 par les enseignants du
cours d'initiation à la programmation de l'IUT de Champs-sur-Marne (Arnaud
Carayol, Cyril Nicaud, Carine Pivoteau), ultérieurement rebaptisé ``upemtk``
pour une utilisation en Licence d'Informatique à l'UPEM jusqu'en 2019-2020.
Avec le changement de nom de l'UPEM, dorénavant Université Gustave Eiffel, le
module change encore de nom et devient "EiffelTk (``fltk``)".

Cette documentation vous guidera pour l'écriture de petits programmes
utilisant les fonctions de dessin et de traitement d'événements
fournies par ``fltk``.

Table des matières :
----------------------------------

.. toctree::
   :maxdepth: 2

   intro
   dessins
   effacer
   evenements

.. Indices and tables
   ==================
   * :ref:`genindex`
   * :ref:`modindex`
   * :ref:`search`
       
