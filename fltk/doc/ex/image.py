from fltk import *

cree_fenetre(400, 400)

# sans redimensionnement
image(133, 200, 'smiley.gif', ancrage='center', tag='im')

# avec redimensionnement
image(316, 200, 'smiley.gif',
      largeur=100, hauteur=400, ancrage='center', tag='im')

attend_ev()
efface('im')
ferme_fenetre()
